package main

import (
	"fmt"

	ci "gitlab.com/mitchenielsen/gitlab-ci-go"
)

const (
	defaultImage        = "docker:24.0.5"
	defaultServiceImage = "docker:24.0.5-dind"
)

var (
	vars = map[string]string{
		"DOCKER_TLS_CERTDIR": "",
	}
)

func main() {
	result := ci.CI{
		Globals: ci.Globals{
			Stages:    []string{"build", "build_and_push"},
			Variables: vars,
			Default: ci.Default{
				Image: defaultImage,
				Services: []ci.Service{
					{Name: defaultServiceImage},
				},
				BeforeScript: []string{
					"docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY",
					"docker pull $CI_REGISTRY_IMAGE:latest || true",
				},
			},
		},
		Jobs: map[string]ci.JobConfig{
			"build": {
				Stage: "build",
				Script: []string{
					dockerBuild(),
				},
				Rules: []ci.WorkflowRule{
					{
						If: ci.RuleIsNotDefaultBranch,
					},
				},
			},
			"build_and_push": {
				Stage: "build_and_push",
				Script: []string{
					dockerBuild(),
					dockerPush("$CI_COMMIT_SHA"),
					dockerPush("latest"),
				},
				Rules: []ci.WorkflowRule{
					{
						If: ci.RuleIsDefaultBranch,
					},
				},
			},
		},
	}

	result.Write()
}

func dockerBuild() string {
	return "docker build --cache-from $CI_REGISTRY_IAMGE:latest --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA --tag $CI_REGISTRY_IMAGE:latest ."
}

func dockerPush(tag string) string {
	return fmt.Sprintf("docker push $CI_REGISTRY_IMAGE:%s", tag)
}
