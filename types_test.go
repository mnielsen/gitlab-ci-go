package ci

import (
	_ "embed"
	"testing"
)

var (
	//go:embed testdata/test1.txt
	golden1 string

	//go:embed testdata/test2.txt
	golden2 string

	//go:embed testdata/test3.txt
	golden3 string
)

func goldens() []string {
	return []string{golden1, golden2, golden3}
}

func TestBuild(t *testing.T) {
	tests := []CI{
		{
			Globals: Globals{
				Stages: []string{"build", "test"},
				Default: Default{
					Image: "alpine:latest",
				},
			},
			Jobs: map[string]JobConfig{
				"package build": {
					Stage:  "build",
					Image:  "alpine:latest",
					Script: []string{"echo", "hello world!"},
				},
				"package test": {
					Stage:  "test",
					Image:  "test:latest",
					Script: []string{"./test.sh"},
				},
			},
		},
		{
			Globals: Globals{
				Stages: []string{"build"},
			},
			Jobs: map[string]JobConfig{
				"package build": {
					Stage:  "build",
					Image:  "alpine:latest",
					Script: []string{"echo", "hello world!"},
				},
			},
		},
		{
			Globals: Globals{
				Stages: []string{"build"},
			},
			Jobs: map[string]JobConfig{
				"package build": {
					Stage:  "build",
					Script: []string{"echo", "hello world!"},
				},
			},
		},
	}

	for i := range tests {
		result, err := tests[i].Build()
		if err != nil {
			t.Fatalf("unable to build config: %v", err)
		}

		if result != goldens()[i] {
			t.Errorf("expected:\n %s\n\ngot:\n %s\n", goldens()[i], result)
		}
	}
}
