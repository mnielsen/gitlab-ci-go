# gitlab-ci-go

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/mitchenielsen/gitlab-ci-go.svg)](https://pkg.go.dev/gitlab.com/mitchenielsen/gitlab-ci-go)

Write GitLab CI pipelines using Golang.

## Why?

As YAML is only a markup langauge and not a full-blown
programming language, it has some inherent limitations.

This Go library allows you to write the CI configuration
with all the benefits of a programming language behind you,
meaning:

- Development environments can be used to better map, trace,
  and debug configuration.
- Easily share configuration using Go modules.
- Configuration can be DRY (without meddling with anchors).
- You aren't focused on counting spaces in YAML.
- It's easier to write tests.

This approach is similar to
[Pulumi's Cloud Native SDK](https://www.pulumi.com/docs/get-started/kubernetes/review-project/)
which supports constructing Kubernetes manifests using many different languages,
including Go.

## Known limitations

Only a subset of [GitLab CI options](https://docs.gitlab.com/ee/ci/yaml)
are exposed by this package. It's fairly straightforward to add more, but
this package will naturally lag behind all supported features that are defined
in the [GitLab project](https://gitlab.com/gitlab-org/gitlab).

Because GitLab CI is written in Ruby, this package needs to translate options
into Golang. If GitLab CI were ever written in Golang instead, we could leverage
that code and not need to redefine all of the options into Go structs.

In its current state, and given the limitations above, this package is only
helpful for projects with a fairly straightforward CI implementation since not
all configuration options are exposed here. Additionally, this is currently a
side project and therefore not officially supported. I recommend picking a tagged
release to avoid any unexpected changes.

## How to use it

### Bootstrap project

Run the following from the root of your project:

```plaintext
mkdir -p ./ci
cd ci
go mod init <GitLab URL>/<project path>/ci
touch generate.go
```

Add your CI configuration in `generate.go`.
Below is a sample starting configuration:

```golang
package main

import (
    ci "gitlab.com/mitchenielsen/gitlab-ci-go"
)

func main() {
    result := ci.CI{
        // Configure CI here.
    }

    result.Lint() // optional

    result.Write()
}
```

For all available configuration, see the
[package documentation](https://pkg.go.dev/gitlab.com/mitchenielsen/gitlab-ci-go).

For a fully working example, see
[`examples/generate.go`](examples/generate.go).

### Generate `.gitlab-ci.yml`

There are two options for generating the `.gitlab-ci.yml` file from your
Golang-based configuration.

### Option 1: Generate and commit

The first option is to generate the CI pipeline manually and comit the `.gitlab-ci.yml`
file as usual.

For this option, simply run:

```plaintext
cd ci/
go run generate.go
```

The CI file will be written to `../.gitlab-ci.yml`, overwriting the file if
it exists already. You can change the file path by configuring `CI_FILE_PATH`.
You can then commit the changes as usual.

### Option 2: Dynamic child pipelines

This package leverages GitLab's
[Dynamic child pipelines](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#dynamic-child-pipelines)
feature, which allows you to generate a CI configuration file and use it in a
child pipeline.

First, create a CI pipeline in `.gitlab-ci.yml`:

```yaml
include:
  - project: 'mitchenielsen/gitlab-ci-go'
    file: '/ci/templates/.gitlab-ci.yml'
```

When these changes are pushed to your project, you will see CI run the `generate-config`
job. This runs `go run generate.go` from your `ci` directory, writing the output
to a new file named `generated-config.yml`. This file is saved as a CI artifact.

Next, the child pipeline will run using the `generated-config.yml` file as the
source for the pipeline configuration.

That's it!

## References

- [GitLab CI source code](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/)
  - Source of truth for CI configuration, written in Ruby
- [firecow/gitlab-ci-local](https://github.com/firecow/gitlab-ci-local/blob/master/src/job.ts)
  - Here, the building blocks of GitLab CI are redefined in Rust, similar to
    what this project does in Golang
- [ar2gostruct](https://github.com/t-k/ar2gostruct)
  - Interesting project that "Generates Go (golang) Structs from activerecord models"
  - Can be tested on GitLab with the following steps:
    1. Set up the [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit).
    1. Run `cd gitlab` from the GDK root directory.
    1. Add the gem to the Gemfile.
    1. Run `bundle install`.
    1. Run `bundle exec ar2gostruct --a > results.go`.
  - Example, partial output: [snippet 2587185](https://gitlab.com/-/snippets/2587185)
