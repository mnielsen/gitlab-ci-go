package ci

import (
	"log"
	"os"
)

const (
	filePathEnvName = "CI_FILE_PATH"
	filePathDefault = "../.gitlab-ci.yml"
	filePerms       = 0644
)

func getFilePath() string {
	path, set := os.LookupEnv(filePathEnvName)
	if !set {
		path = filePathDefault
	}

	return path
}

// Write will build the pipeline and write the result
// to the specified file path.
func (c CI) Write() error {
	pipeline, err := c.Build()
	if err != nil {
		return err
	}

	filePath := getFilePath()

	err = os.WriteFile(filePath, []byte(pipeline), filePerms)
	if err != nil {
		return err
	}

	log.Printf("wrote CI configuration file to %s\n", filePath)

	return nil
}
